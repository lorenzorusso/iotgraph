package threatmanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import auto_graphdefinition.protocols;
import iotgraph.creategraph;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.graphs.graphModel;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.models.macm.macmRelationshipType;

public class manager{
	private String connectionString=""; 
	private Connection connection = null;
	private String user="root";
	private String password="root";
	ArrayList<String> presented=new ArrayList<String>();
	private ResultSet idprotocol,idthreat;
	ArrayList<String> list_idthreat=new ArrayList<String>();
	
	public manager() {
		this.connectionString="jdbc:mysql://localhost:3306/slagenerator?user="+ this.user +"&password="+ this.password ;
	}
	
	public boolean verify(String input) 
	{
		
			for(String text:this.presented) 
			{
					if(input.equals(text) ) 
					{
						return true;
					}
			}
		return false;
	}
	
	public void connect() 
	{
		try 
		{
		    Class.forName("com.mysql.jdbc.Driver"); //Chiama Driver
		} 
		catch (ClassNotFoundException e)
		{
		    e.printStackTrace();
		}
		try 
		{
		    connection = DriverManager.getConnection(connectionString); //Connetti al DB
		}
		catch(Exception e) 
		{
			System.out.println(e.getMessage());
		}
	}
	
	public void disconnect() 
	{
		try 
		{
			if(connection!=null) 
			{
				connection.close(); //Chiusura connessione
			}
		}
		catch(Exception e) 
		{
			System.out.println(e.getMessage());
		}
	}
	
	public List<threat> query(String name) throws SQLException 
	{
		this.connect();
		Statement statement=connection.createStatement();
		Statement statement2=connection.createStatement();
		List<threat> threatList=new ArrayList<threat>();
		
		String query="SELECT idprotocol FROM protocols WHERE protocol='"+name+"'";
		
		idprotocol=statement.executeQuery(query);
		idprotocol.next(); 
		System.out.println("TROVATO IDPROTOCOL: "+idprotocol.getString(1));
		
		query="SELECT id_threat FROM threatprotocol WHERE id_protocol='"+idprotocol.getString(1)+"';";
		idthreat=statement.executeQuery(query);
		
		while(idthreat.next() ) {
			list_idthreat.add(idthreat.getString(1));
			query="SELECT threat_name, threat_description FROM threats WHERE threat_id='"+idthreat.getString(1)+"';";
			ResultSet result=statement2.executeQuery(query);
			result.next();
			threat one=new threat();
			one.name=result.getString(1);
			one.description=result.getString(2);
			
			
			query="SELECT ease_of_discovery, ease_of_exploit, intrusion_detection, loss_of_accountability, loss_of_availability,"
					+ "loss_of_confidentiality, loss_of_integrity"
					+ " FROM threats WHERE threat_id='"+idthreat.getString(1)+"';";
			result=statement2.executeQuery(query);
			result.next();
			one.properties.put("ease_of_discovery", Integer.parseInt(result.getString(1)));
			one.properties.put("ease_of_exploit",  Integer.parseInt(result.getString(2)));
			one.properties.put("intrusion_detection",  Integer.parseInt(result.getString(3)));
			one.properties.put("loss_of_accountability", Integer.parseInt (result.getString(4)));
			one.properties.put("loss_of_availability",Integer.parseInt( result.getString(5)));
			one.properties.put("loss_of_confidentiality", Integer.parseInt(result.getString(6)));
			one.properties.put("loss_of_integrity", Integer.parseInt(result.getString(7)));
			
			
			query="SELECT  motive, opportunity, size, skill_level"
					+ " FROM threats WHERE threat_id='"+idthreat.getString(1)+"';";
			result=statement2.executeQuery(query);
			result.next();
			one.properties.put("motive", Integer.parseInt(result.getString(1)));
			one.properties.put("opportunity", Integer.parseInt(result.getString(2)));
			one.properties.put("size", Integer.parseInt(result.getString(3)));
			one.properties.put("skill_level", Integer.parseInt(result.getString(4)));
			
			query="SELECT  id FROM threats WHERE threat_id='"+idthreat.getString(1)+"';";
			result=statement2.executeQuery(query);
			result.next();
			//System.out.println("id della tab threat: "+result.getString(1));
			query="SELECT  strides_id FROM threats_strides WHERE threat_id='"+result.getString(1)+"';";
			result=statement2.executeQuery(query);
			if(result.next()) {
				query="SELECT  name FROM strides WHERE id='"+result.getString(1)+"';";
				result=statement2.executeQuery(query);
				if(result.next()) {
				one.strides= result.getString(1);
				}
			}
			
			
			
			threatList.add(one);
//			System.out.println(result.getString(1));
//			System.out.println(result.getString(2)+"\n");
		}
		return threatList;
	}
	
	public List<threat> run() throws SQLException {
		String protocol, type;
		//manager man = new manager();
		creategraph t = new creategraph();
		List<threat> threat=new ArrayList<threat>();
		Map<String, Peer> lista = t.peer();
		List<Relationship> Listarel=t.rel();
		System.out.println("\nInizio Ricerca Threat");

		for (Peer p : lista.values()) {
			protocol = p.getProperty("protocol");
			//System.out.println("TROVATO PROTOCOLLO: "+protocol);
			if (!this.verify(protocol)) // Verifica se protocollo gia inserito nell elenco threat da presentare
			{
				this.presented.add(protocol);

				switch (protocol) {
				case "xmpp":
					System.out.println("THREAT XMPP");
					threat.addAll(this.query("xmpp"));
					break;
				case "http":
					System.out.println("THREAT HTTP");
					threat.addAll(this.query("http"));
					break;
				case "zigbee":
					System.out.println("THREAT ZIGBEE");
					threat.addAll(this.query("zigbee"));
					break;
				case "ssl":
					System.out.println("THREAT SSL");
					threat.addAll(this.query("tls/ssl"));
					break;
				case "ethernet":
					System.out.println("THREAT ETHERNET");
					threat.addAll(this.query("ethernet"));
					break;
				case "https":
					threat.addAll(this.query("ssl"));
					threat.addAll(this.query("http"));
					break;
				case "tcp":
					System.out.println("THREAT TCP");
					threat.addAll(this.query("tcp"));
					break;
				case "ip":
					System.out.println("THREAT IP");
					threat.addAll(this.query("ip"));
					break;
				}
			}
		}
		for(Relationship r:Listarel) {
			if(r.getProperty().getProperty("protocol")!=null) {
				protocol=r.getProperty().getProperty("protocol");
				//System.out.println("Trovato Protocollo per relaz: "+protocol);
			}
			else protocol="";
				if (!this.verify(protocol)) // Verifica se protocollo gia inserito nell elenco threat da presentare
			{
				this.presented.add(protocol);

				switch (protocol) {
				case "xmpp":
					System.out.println("THREAT XMPP");
					threat.addAll(this.query("xmpp"));
					break;
				case "http":
					System.out.println("THREAT HTTP");
					threat.addAll(this.query("http"));
					break;
				case "zigbee":
					System.out.println("THREAT ZIGBEE");
					threat.addAll(this.query("zigbee"));
					break;
				case "ssl":
					System.out.println("THREAT SSL");
					threat.addAll(this.query("ssl"));
					break;
				case "ethernet":
					System.out.println("THREAT ETHERNET");
					threat.addAll(this.query("ethernet"));
					break;
				case "https":
					threat.addAll(this.query("ssl"));
					threat.addAll(this.query("http"));
					break;
				case "tcp":
					System.out.println("THREAT TCP");
					threat.addAll(this.query("tcp"));
					break;
				case "ip":
					System.out.println("THREAT IP");
					threat.addAll(this.query("ip"));
					break;
				}
			}
		}
		return threat;
	}
	
	
}








