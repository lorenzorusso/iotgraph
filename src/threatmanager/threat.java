package threatmanager;


import java.util.HashMap;
import java.util.Map;

public class threat {
	public String name;
	public String description;
	public String riskSeverity="n";
	public  Map<String, Integer> properties = new HashMap<>();
	public Map<String, Integer> risk=new HashMap<>();
	public String strides="a";
	
	public threat() {
		risk.put("impact", 5);
		risk.put("likehood", 5);
		//risk.put("riskSeverity", 5.00);
		risk.put("skillLevel", 6);
		//risk.put("riskSeverity", 0.00);
		
		properties.put("financial_damage", 5);
		properties.put("reputation_damage", 5);
		properties.put("non_compliance", 5);
		properties.put("privacy_violation", 5);
		properties.put("awareness", 5);
		
	}
}
