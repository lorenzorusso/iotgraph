package controlManager;

import java.util.HashMap;
import java.util.Map;

public class SecurityControl {
	String id;
	String name;
	String description;
	String threat;
	public String selected="NO";
	public  Map<String, String> properties = new HashMap<>();
	public SecurityControl(String id,String name,String desc,String threat){
		this.id=id;
		this.name=name;
		this.description=desc;
		this.threat=threat;
	}
	public SecurityControl() {
		
	}

}
