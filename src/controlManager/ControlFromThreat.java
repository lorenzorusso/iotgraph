package controlManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ControlFromThreat {
	
	
	public List<SecurityControl> searchControl(String threat,String rischio) throws ClassNotFoundException, SQLException, JSONException {
		List<SecurityControl> controls=new ArrayList<SecurityControl>();
		String user="root";
		String password="root";
		String connectionString="jdbc:mysql://localhost:3306/slagenerator?user="+ user +"&password="+ password ;
		Class.forName("com.mysql.jdbc.Driver");
		Connection connection=DriverManager.getConnection(connectionString);
		Statement statement2=connection.createStatement();
		Statement statement3=connection.createStatement();
		
		String query="SELECT id FROM threats WHERE threat_name='"+threat+"';";
		ResultSet result_control=statement2.executeQuery(query);
		result_control.next();
		String idthreat=result_control.getString(1);
		//System.out.println("Trovato ID threat: "+idthreat);
		query="SELECT suggested_controls_id FROM threats_suggested_controls WHERE threat_id='"+idthreat+"';";
		result_control=statement2.executeQuery(query);
		
		while(result_control.next()) {
			
			String idcontrol=result_control.getString(1);
			System.out.println(idcontrol);
			query="SELECT control_id, control_name, control_description FROM controls WHERE id='"+idcontrol+"';";
			ResultSet result=statement3.executeQuery(query);
			result.next();
			String id= result.getString(1);
			String name= result.getString(2);
			String description= result.getString(3);
			//SecurityControl control=new SecurityControl(id,name,description,threat);
			SecurityControl control=new SecurityControl();
			control.properties.put("name", name);
			control.properties.put("id", id);
			control.properties.put("description", description);
			control.properties.put("risk", 	rischio);
//			Boolean insert=true;
//			String temp;
//			for(int i=0;i<controls.size();i++) {
//				temp=controls.get(i).properties.get("id");
//				if(temp.equals(id)) {
//					insert=false;
//				}else {}
//			}	
//			if(insert==true)	{ controls.add(control); }
			controls.add(control);
		}
		
		return controls;
	}

}
