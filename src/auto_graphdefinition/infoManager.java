package auto_graphdefinition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.utilities.NeoDriver;

//public class infoManager {
//	
//	
//
//	private String user="neo4j";
//	private String pass="max";
//	private Driver driver;
//	private Session session;
//	private Peer p;
//	private String statement="";
//	private StatementResult result=null;
//	final static Logger logger = Logger.getLogger(relations.class);
//	private List<PhraseInfo> phrases=new ArrayList<PhraseInfo>();
//
//	public List<PhraseInfo> get_phrases()
//	{
//		this.get_uses();
//		this.get_connects();
//		PhraseInfo r=new PhraseInfo();
//		r.resetid();
//		return this.phrases;
//	}
//	
//	
//	public void get_protocols() 
//	{
//		StatementResult temp=null;
//		String start,stop,relation,return_prot,return_type;
//		Record record,record_type ;
//		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user, pass));
//		this.session=driver.session();
//		
//		//Recupero Nodi Relazione Uses
//		statement ="MATCH (a),(b),(a)-[r:uses]->(b) return a.name AS START,TYPE(r) as RELATION,b.name AS STOP";
//		result=session.run(statement);
//		logger.debug("AVVIO WIZARD RELAZIONI \"USES\"");
//		while ( result.hasNext() ) 
//		{
//			record = result.next();
//			start=record.get("START").asString();
//			stop=record.get("STOP").asString();
//			relation=record.get("RELATION").asString();
//			
//			//Recupera protocollo
//			statement ="MATCH ((a)-[r:uses]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' return r.protocol as prot";
//			temp=session.run(statement);
//			return_prot=temp.next().get("prot").asString();			
//			
//			//Verifica se protocollo assente o errato
//			if(return_prot==null || ( verify_prot(return_prot)==false))
//			{
//				//Richiesta inserimento protocollo
//				PhraseInfo phrase=new PhraseInfo();
//				phrase.phrase="\""+start+"\""+ " "+relation.toUpperCase()+" \""+stop+"\". Definire PROTOCOLLO per relazione "+relation.toUpperCase();
//				phrase.rel=relation;
//				phrase.start=start;
//				phrase.stop=stop;
//				phrases.add(phrase);
//				
//			}
//			
////			//Definizione Attributi Nodi
////			statement ="MATCH ((a)-[r:uses]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' return a.type as atype, b.type as btype";
////			temp=session.run(statement);
////			record_type=temp.next();
////			return_type=record_type.get("atype").asString();
//			
//			//Nodo START
////			while( return_type==null || verify_type(return_type)==false )
////			{
////				System.out.println("Inserire \"ROLE\" per nodo "+start);
////				in2=input.nextLine();
////				return_type=in2;
////				logger.debug("This is debug. Inserted: "+in2);
////				statement ="MATCH ((a)-[r:uses]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' set a.type='"+in2+"'";
////				session.run(statement);
////			}
//			
////			if(return_type==null || verify_type(return_type)==false) 
////			{
////				//phraseUses.add("\"ROLE\" per nodo START "+start +" in relazione USES con "+stop);
//////				phrase.add(start.toUpperCase() +" uses "+stop+". Definire ROLE "+start);
////
////				PhraseInfo phrase=new PhraseInfo();
////				phrase.phrase="\""+start.toUpperCase() +"\" uses \""+stop+"\". Definire ROLE \""+start+"\"";
////				phrase.rel=relation;
////				phrase.start=start;
////				phrase.stop=stop;
////				phrases.add(phrase);
////			}
//			
////			return_type=record_type.get("btype").asString();
//			
//			//Nodo STOP
////			while(return_type==null || verify_type(return_type)==false )
////			{	
////				System.out.println("Inserire \"role\" per nodo "+stop);
////				in2=input.nextLine();
////				return_type=in2;
////				logger.debug("This is debug. Inserted: "+in2);
////				statement ="MATCH ((a)-[r:uses]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' set b.type='"+in2+"'";
////				session.run(statement);
////			}
//			
////			if(return_type==null || verify_type(return_type)==false) 
////			{
////				phraseUses.add("\"ROLE\" per nodo STOP "+stop+" in relazione USES con "+start);
////				phrase.add(start +" uses "+stop.toUpperCase()+". Definire ROLE "+stop);
////				PhraseInfo phrase=new PhraseInfo();
////				phrase.phrase="\""+start +"\" uses \""+stop.toUpperCase()+"\". Definire ROLE \""+stop+"\"";
////				phrase.rel=relation;
////				phrase.start=start;
////				phrase.stop=stop;
////				phrases.add(phrase);
////				
////			}
//			//Recupero Nodi Relazione connects
//			statement ="MATCH (a),(b),(a)-[r:connects]->(b) return a.name AS START,TYPE(r) as RELATION,b.name AS STOP";
//			result=session.run(statement);
//			while(result.hasNext()) {
//				
//				record = result.next();
//				start=record.get("START").asString();
//				stop=record.get("STOP").asString();
//				relation=record.get("RELATION").asString();
//				//Recupera protocollo
//				statement ="MATCH ((a)-[r:connects]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' return r.protocol as prot";
//				temp=session.run(statement);
//				return_prot=temp.next().get("prot").asString();
//				//Verifica se protocollo mancante o errato
//				if(return_prot==null || ( verify_prot(return_prot)==false)) 
//				{
//					//Richiesta info protocollo
//					PhraseInfo phrase=new PhraseInfo();
//					phrase.phrase="\""+start+"\" "+relation.toUpperCase()+" \""+stop+"\". Definire PROTOCOLLO relazione";
//					phrase.rel=relation;
//					phrase.start=start;
//					phrase.stop=stop;
//					phrases.add(phrase);
//					
//					
//				}
//				
//			}
//			
//		}//Fine while
//		this.session.close(); //Chiusura sessione
//		logger.debug("SESSION CLOSED");
//		
//	}
//
//	
//	
//	public void get_connects() 
//	{
//		StatementResult temp=null;
//		String start,stop,relation, in,in2,return_prot,return_type;
//		Record record,record_type ;
//		Scanner input = new Scanner(System.in);
//		in="";
//		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user,pass));
//		this.session=driver.session();
//		
////		//Recupero Nodi Relazione connects
////		statement ="MATCH (a),(b),(a)-[r:connects]->(b) return a.name AS START,TYPE(r) as RELATION,b.name AS STOP";
////		result=session.run(statement);
////		logger.debug("AVVIO WIZARD RELAZIONI \"CONNECTS\"");
////		while ( result.hasNext() ) 
////		{
////			record = result.next();
////			start=record.get("START").asString();
////			stop=record.get("STOP").asString();
////			relation=record.get("RELATION").asString();
////			
////			//Recupera protocollo
////			statement ="MATCH ((a)-[r:connects]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' return r.protocol as prot";
////			temp=session.run(statement);
////			return_prot=temp.next().get("prot").asString();
////			//System.out.println(start+" - "+relation+" - "+stop);
////			while(return_prot==null || ( verify_prot(return_prot)==false) )
////			{	
////				//Set protocollo della relazione connects
////				System.out.println("Definire il PROTOCOLLO per relazione "+relation+ " tra "+start+" e "+stop);
////				in= input.nextLine();
////				return_prot=in;
////				//System.out.println("INSERTED: "+in);
////				logger.debug("Inserted: "+in);
////				//Scrittura in neo4j
////				statement ="MATCH ((a)-[r:connects]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' set r.protocol='"+in+"'";
////				session.run(statement);
////			}
////			
////			if(return_prot==null || ( verify_prot(return_prot)==false)) 
////			{
////				//phraseConnects.add("PROTOCOLLO per relazione "+relation.toUpperCase()+ " tra "+start+" e "+stop);
////				phrase.add(start+" "+relation.toUpperCase()+" "+stop+". Definire PROTOCOLLO relazione");
////				PhraseInfo phrase=new PhraseInfo();
////				phrase.phrase="\""+start+"\" "+relation.toUpperCase()+" \""+stop+"\". Definire PROTOCOLLO relazione";
////				phrase.rel=relation;
////				phrase.start=start;
////				phrase.stop=stop;
////				phrases.add(phrase);
////				
////				
////			}
//			
//			//Definizione Attributi Nodi
////			statement ="MATCH ((a)-[r:connects]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' return a.type as atype, b.type as btype";
////			temp=session.run(statement);
////			record_type=temp.next();
////			return_type=record_type.get("atype").asString();
//		
////			Recupero Nodi Relazione connects
//			statement ="MATCH (a),(b),(a)-[r:connects]->(b) return a.name AS START,TYPE(r) as RELATION,b.name AS STOP";
//			result=session.run(statement);
//			
//			//Nodo START
////			while( return_type==null || verify_type(return_type)==false  )
////			{
////				System.out.println("Inserire \"role\" per nodo "+start.toUpperCase());
////				in2=input.nextLine();
////				return_type=in2;
////				logger.debug("Inserted: "+in2);
////				statement ="MATCH ((a)-[r:connects]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' set a.type='"+in2+"'";
////			}
//			
//			if(return_type==null || verify_type(return_type)==false ) 
//			{
////				phraseConnects.add("ROLE per nodo "+start.toUpperCase());
////				phrase.add(start.toUpperCase()+" CONNECTS "+stop+". Definire ROLE per "+start);
//
//				PhraseInfo phrase=new PhraseInfo();
//				phrase.phrase="\""+start.toUpperCase()+"\" connects \""+stop+"\". Definire ROLE per \""+start+"\"";
//				phrase.rel=relation;
//				phrase.start=start;
//				phrase.stop=stop;
//				phrases.add(phrase);
//				
//				
//			}
//			
//			//session.run(statement);
//			return_type=record_type.get("btype").asString();
//			
//			//Nodo STOP
////			while(return_type==null || verify_type(return_type)==false )
////			{	
////				System.out.println("Inserire \"role\" per nodo "+stop.toUpperCase());
////				in2=input.nextLine();
////				return_type=in2;
////				logger.debug("Inserted: "+in2);
////				statement ="MATCH ((a)-[r:connects]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' set b.type='"+in2+"'";
////			}
////			session.run(statement);
//			
//			if(return_type==null || verify_type(return_type)==false) 
//			{
////				phraseConnects.add("ROLE per nodo "+stop.toUpperCase());
////				phrase.add(start+" connects "+stop.toUpperCase()+". Definire ROLE per nodo "+stop);
//
//				PhraseInfo phrase=new PhraseInfo();
//				phrase.phrase="\""+start+"\" connects \""+stop.toUpperCase()+"\". Definire ROLE per nodo \""+stop+"\"";
//				phrase.rel=relation;
//				phrase.start=start;
//				phrase.stop=stop;
//				phrases.add(phrase);
//				
//			}
//			
//			
//		}//Fine while
//		this.session.close(); //Chiusura sessione
//		logger.debug("SESSION CLOSED");
//		
//	}
//	
//	public void delete() throws IOException 
//	{
//		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user,pass));
//		this.session=driver.session();
//		//Recupero Nodi Relazione Uses
//		NeoDriver.cleanDB(); //Cancella tutti i grafi esistenti
//		logger.debug("DATABASE DELETED");
//	}
//	
//	public void runcipher(String cipher) throws IOException 
//	{
//		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user,pass));
//		this.session=driver.session();
//		//Recupero Nodi Relazione Uses
//		//NeoDriver.executeFromFile(path); //Esegue il cipher code
//		NeoDriver.execute(cipher);
//		logger.debug("GRAPH CREATED");
//	}
//	
//	public void runcipher() throws IOException 
//	{
//		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user,pass));
//		this.session=driver.session();
//		//Recupero Nodi Relazione Uses
//		NeoDriver.executeFromFile("./file/scenariovuoto"); //Esegue il cipher code
////		NeoDriver.execute(cipher);
//		logger.debug("GRAPH CREATED");
//	}
//	
//	boolean verify_prot(String input)
//	{
//		for(protocols p:protocols.values())
//		{
//			if (p.toString().equals(input)) 
//			{
//				logger.debug("Ok protocols "+input);
//				return true;
//			}
//		}
//		logger.debug("Protocol not found "+input);
//		return false;
//	}
//	
//	boolean verify_type(String input)
//	{
//		for(type p:type.values())
//		{
//			if (p.toString().equals(input)) 
//			{
//				logger.debug("Ok type: "+input);
//				return true;
//			}
//		}
//		logger.debug("Type not found: "+input);
//		return false;
//	}
//	
//
//}
