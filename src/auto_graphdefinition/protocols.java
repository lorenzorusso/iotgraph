package auto_graphdefinition;

public enum protocols 
{
	xmpp,
	ssl,
	tls,
	zigbee,
	tcp,
	ip,
	http,
	https,
	oauth,
	ethernet;
}
