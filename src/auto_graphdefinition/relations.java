package auto_graphdefinition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.jenetics.util.Verifiable;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.utilities.NeoDriver;
import iotgraph.creategraph;

public class relations 
{
	private String user="neo4j";
	private String pass="max";
	private Driver driver;
	private Session session;
	private Peer p;
	private String statement="";
	private StatementResult result=null;
	final static Logger logger = Logger.getLogger(relations.class);


	private List<PhraseInfo> phrases=new ArrayList<PhraseInfo>();

	public List<PhraseInfo> get_phrases()
	{
		this.get_uses();
		this.get_connects();
		PhraseInfo r=new PhraseInfo();
		r.resetid();

		return this.phrases;
	}
	
	
	public void get_uses() 
	{
		StatementResult temp=null;
		String start,stop,relation, in,in2,return_prot,return_type;
		Record record,record_type ;
		Scanner input = new Scanner(System.in);
		in="";
		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user, pass));
		this.session=driver.session();
		
		//Recupero Nodi Relazione Uses
		statement ="MATCH (a),(b),(a)-[r:uses]->(b) return a.name AS START,TYPE(r) as RELATION,b.name AS STOP";
		result=session.run(statement);
		logger.debug("AVVIO WIZARD RELAZIONI \"USES\"");
		while ( result.hasNext() ) 
		{
			record = result.next();
			start=record.get("START").asString();
			stop=record.get("STOP").asString();
			relation=record.get("RELATION").asString();
			
			//Verifica se protocollo gia presente
			statement ="MATCH ((a)-[r:uses]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' return r.protocol as prot";
			temp=session.run(statement);
			return_prot=temp.next().get("prot").asString();			

			
			if(return_prot==null || ( verify_prot(return_prot)==false))
			{
				
				Protocol phrase=new Protocol();
				phrase.phrase=start+" "+relation.toUpperCase()+" "+stop+". Definire PROTOCOLLO per relazione "+relation.toUpperCase();
				phrase.rel=relation;
				phrase.start=start;
				phrase.stop=stop;
				phrases.add(phrase);
				
			}
			
			//Definizione Attributi Nodi
			statement ="MATCH ((a)-[r:uses]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' return a.type as atype, b.type as btype";
			temp=session.run(statement);
			record_type=temp.next();
			return_type=record_type.get("atype").asString();
			
			
			if(return_type==null || verify_type(return_type)==false) 
			{

				Role phrase=new Role();
				phrase.phrase=start.toUpperCase() +" uses "+stop+". Definire ROLE "+start+"\"";
				phrase.rel=relation;
				phrase.start=start;
				phrase.stop=stop;
				phrase.actor="start";
				phrases.add(phrase);
			}
			
			return_type=record_type.get("btype").asString();
			
			if(return_type==null || verify_type(return_type)==false) 
			{
				Role phrase=new Role();
				phrase.phrase=start +" uses "+stop.toUpperCase()+". Definire ROLE "+stop;
				phrase.rel=relation;
				phrase.start=start;
				phrase.stop=stop;
				phrase.actor="stop";
				phrases.add(phrase);			
			}
			
		}//Fine while
		this.session.close(); //Chiusura sessione
		logger.debug("SESSION CLOSED");	
	}
	
	
	
	public void get_connects() 
	{
		StatementResult temp=null;
		String start,stop,relation, in,in2,return_prot,return_type;
		Record record,record_type ;
		Scanner input = new Scanner(System.in);
		in="";
		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user,pass));
		this.session=driver.session();
		
		//Recupero Nodi Relazione connects
		statement ="MATCH (a),(b),(a)-[r:connects]->(b) return a.name AS START,TYPE(r) as RELATION,b.name AS STOP";
		result=session.run(statement);
		logger.debug("AVVIO WIZARD RELAZIONI \"CONNECTS\"");
		while ( result.hasNext() ) 
		{
			record = result.next();
			start=record.get("START").asString();
			stop=record.get("STOP").asString();
			relation=record.get("RELATION").asString();
			
			statement ="MATCH ((a)-[r:connects]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' return r.protocol as prot";
			temp=null;
			temp=session.run(statement);
			return_prot=temp.next().get("prot").asString();
			
			if(return_prot==null || ( verify_prot(return_prot)==false)) 
			{
				Protocol phrase=new Protocol();
				phrase.phrase=start+" "+relation.toUpperCase()+" "+stop+". Definire PROTOCOLLO relazione";
				phrase.rel=relation;
				phrase.start=start;
				phrase.stop=stop; 
				phrases.add(phrase);	
			}
			
			//Definizione Attributi Nodi
			statement ="MATCH ((a)-[r:connects]->(b)) WHERE a.name='"+start+"' and b.name='"+stop+"' return a.type as atype, b.type as btype";
			temp=null;
			temp=session.run(statement);
			record_type=temp.next();
			return_type=record_type.get("atype").asString();
			
			if(return_type==null || verify_type(return_type)==false ) 
			{			
				Role phrase=new Role();
				phrase.phrase=start.toUpperCase()+" connects "+stop+". Definire ROLE per "+start;
				phrase.rel=relation;
				phrase.start=start;
				phrase.stop=stop;
				phrase.actor="start";
				phrases.add(phrase);		
			}
			
			//session.run(statement);
			return_type=record_type.get("btype").asString();
			
			if(return_type==null || verify_type(return_type)==false) 
			{
				Role phrase=new Role();
				phrase.phrase=start+" connects "+stop.toUpperCase()+". Definire ROLE per nodo "+stop;
				phrase.rel=relation;
				phrase.start=start;
				phrase.stop=stop;
				phrase.actor="stop";
				phrases.add(phrase);				
			}
		
		}//Fine while
		this.session.close(); //Chiusura sessione
		logger.debug("SESSION CLOSED");		
	}
	
	public boolean set_Role(List<PhraseInfo> phrases, JSONObject j) throws JSONException {
		String tempStart, tempStop, tempRel, SelectedBox;
		int tempId;

		this.driver = GraphDatabase.driver("bolt://localhost:7687", AuthTokens.basic(user, pass));
		this.session = driver.session();

		
		Iterator<PhraseInfo> ph = phrases.iterator();// Iteratore su oggetto List<PhraseInfo>

		while (ph.hasNext()) // Iterazione su id
		{
			PhraseInfo phrase = ph.next();
			tempId = phrase.id;
			String IdString = Integer.toString(tempId); // Indice phrase
			//System.out.print("IdString: "+IdString+" ");
			Iterator<?> keys = j.keys(); // Iteratore su oggetto json
			while (keys.hasNext()) {
				String key = (String) keys.next(); // Fissiamo un indice json
				//System.out.println("key: "+key);
				if (j.get(key) instanceof JSONObject) // Se è un oggetto Json
				{
					JSONObject temp = (JSONObject) j.get(key); // Salvo l'ggetto
					Iterator<?> keys2 = temp.keys(); // Iteratore dell'oggetto interno

					while (keys2.hasNext()) // Iteriamo
					{
						String key2 = (String) keys2.next(); // Fisso in indice dell'oggetto json
						SelectedBox = temp.getString(key2); // recuperiamo un valore dell'oggetto json
						
						if (phrase.getClass().equals(Protocol.class) && IdString.equals(key)) {
							System.out.println("Trovato Protocol");
							tempStart = phrase.start;
							tempStop = phrase.stop;
							tempRel = phrase.rel;
							String set = "MATCH ((a)-[r:" + tempRel + "]->(b)) WHERE a.name='" + tempStart
									+ "' and b.name='" + tempStop + "' set r.protocol='" + SelectedBox + "'";
							session.run(set);
//							System.out.println(tempStart + " " + tempStop + " " + set);

						} else if (phrase.getClass().equals(Role.class) && IdString.equals(key)) {
							
							Role role=(Role)phrase;
							System.out.println("Trovato Role");
							tempStart = phrase.start;
							tempStop = phrase.stop;
							tempRel = phrase.rel;
							String set;
							if(role.actor.equals("start"))
							{
								set = "MATCH ((a)-[r:" + tempRel + "]->(b)) WHERE a.name='" + tempStart
										+ "' and b.name='" + tempStop + "' set a." + SelectedBox + "_role" + "='"
										+ SelectedBox + "'";
								session.run(set);
							}
							else {
								set = "MATCH ((a)-[r:" + tempRel + "]->(b)) WHERE a.name='" + tempStart
										+ "' and b.name='" + tempStop + "' set b." + SelectedBox + "_role" + "='"
										+ SelectedBox + "'";
								session.run(set);
							}
						}
					}//Fine while json interno

				}//Chiusura if json interno
			}//Fine while su jsonObject

		}//Fine while esterno su phrases
		return true;
	}
	
	
	public void delete() throws IOException 
	{
		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user,pass));
		this.session=driver.session();
		//Recupero Nodi Relazione Uses
		NeoDriver.cleanDB(); //Cancella tutti i grafi esistenti
		logger.debug("DATABASE DELETED");
	}
	
	public void runcipher(String cipher) throws IOException 
	{
		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user,pass));
		this.session=driver.session();
		NeoDriver.execute(cipher);
		logger.debug("GRAPH CREATED");
	}
	
	public void runcipher() throws IOException 
	{
		this.driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic(user,pass));
		this.session=driver.session();
		NeoDriver.executeFromFile("./file/scenariovuoto"); //Esegue il cipher code
		logger.debug("GRAPH CREATED");
	}
	
	boolean verify_prot(String input)
	{
		for(protocols p:protocols.values())
		{
			if (p.toString().equals(input)) 
			{
				logger.debug("Ok protocols "+input);
				return true;
			}
		}
		logger.debug("Protocol not found "+input);
		return false;
	}
	
	boolean verify_type(String input)
	{
		for(type p:type.values())
		{
			if (p.toString().equals(input)) 
			{
				logger.debug("Ok type: "+input);
				return true;
			}
		}
		logger.debug("Type not found: "+input);
		return false;
	}
	
//	public static void main(String args []) throws IOException 
//	{
//		Scanner input = new Scanner(System.in);
//		String text;
//		relations one=new relations();
//		System.out.println("Scegliere operazione:\n[r]un_Cipher, [d]elete_graph, get_[c]onnects, get_[u]ses, [e]xit");
//		text=input.nextLine();
//		while(!text.equals("e"))
//		{
//			switch (text)
//			{
//			case "r":
//				one.runcipher();
//				System.out.println("Scegliere operazione:\n[r]un_Cipher, [d]elete_graph, get_[c]onnects, get_[u]ses, [e]xit");
//				text=input.nextLine();
//				break;
//				
//			case "d":
//				one.delete();
//				System.out.println("Scegliere operazione:\n[r]un_Cipher, [d]elete_graph, [w]izard_Risk_analysis, [e]xit");
//				text=input.nextLine();
//				break;	
//				
//			case "c":
//				one.get_connects();
//				System.out.println("Scegliere operazione:\n[r]un_Cipher, [d]elete_graph, [w]izard_Risk_analysis, [e]xit");
//				text=input.nextLine();
//				break;	
//				
//			case "u":
//				one.get_uses();
//				System.out.println("Scegliere operazione:\n[r]un_Cipher, [d]elete_graph, [w]izard_Risk_analysis, [e]xit");
//				text=input.nextLine();
//				break;	
//			}	
//		}
//		System.out.println("Fine Wizard");
//		
//		relations iot=new relations();
//		iot.runcipher();
//		
//		ArrayList<String> prova=iot.get_uses();
//		ArrayList<String> prova2=iot.get_connects();
//		System.out.println(prova);
//		System.out.println("FINE --------------------------------------------------------");
//		System.out.println(prova2);
//	}
	
	
	
}



















