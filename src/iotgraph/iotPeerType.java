package iotgraph;

import eu.musaproject.multicloud.models.graphs.TypeInterface;
import eu.musaproject.multicloud.models.macm.macmPeerType;

public enum iotPeerType implements TypeInterface{
	GATEWAY("GATEWAY"),
	NETWORK("NETWORK"),
	SMARTMETER("SMARTMETER"),
	PHYSICALENTITY("PHYSICALENTITY"),
	SOFTWARE("SOFTWARE"),
	PLATFORM("PLATFORM");

	private final String itsName;

	iotPeerType(String name) {
		itsName=name;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.itsName;
	}

	@Override
	public TypeInterface isInType(String t) {
		// TODO Auto-generated method stub
		for (macmPeerType c : macmPeerType.values()) {
			if (c.name().equals(t)) {
				return c;
			}
		}
		return null;
	}

}
