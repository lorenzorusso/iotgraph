package iotgraph;

import java.awt.peer.ListPeer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.management.relation.Relation;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.macm.MACM;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.models.macm.macmRelationshipType;
import eu.musaproject.multicloud.utilities.NeoDriver;

public class creategraph {
	private MACM macm;
	private Map<String,Peer> listPeer;
	private List<Relationship> rl;
	private iotRead read=new iotRead();
	
	public creategraph() {
		// TODO Auto-generated constructor stub
		try {
		macm = new MACM();
		//NeoDriver.cleanDB(); //Cancella tutti i grafi esistenti
		//NeoDriver.executeFromFile("./file/scenario1"); //Esegue il cipher code
		
		read.read_iot_graph();
		
		this.listPeer=read.getPeers();
		this.rl=read.getRelationship();
		
		for(Entry<String, Peer> lp: listPeer.entrySet()) {
			System.out.println(lp.getValue().getName());
			
		}
		for(Relationship r:rl) {
			System.out.println(r.startNode.getName().toString()+ "---"+r.getType()+"-->"+
								r.endNode.getName().toString());
//			Properties p=r.getProperty();
//			System.out.println("Proprieta della relazione: "+p);			
		}

		}catch(Exception e) {System.out.println(e.toString());}
		
	}
	
	
	public Map<String,Peer> peer()
	{
		return listPeer;
	}

	public List<Relationship> rel()
	{
		return rl;
	}
	
	
	
	/*public static void main(String args []) {
		//new creategraph();
	}
	*/
		
}



