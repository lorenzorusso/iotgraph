package iotgraph;

import eu.musaproject.multicloud.models.graphs.TypeInterface;

public enum iotRelationshipType implements TypeInterface {
	USES("USES"),
	CONNECTS("CONNECTS"),
	HOSTS("HOSTS"),
	MONITORS("MONITORS");

	private final String itsName;

	iotRelationshipType(String name) {
		itsName=name;
	}	
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TypeInterface isInType(String t) {
		// TODO Auto-generated method stub
		return null;
	}

}
