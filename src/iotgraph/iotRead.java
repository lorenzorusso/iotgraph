package iotgraph;

import java.util.List;
import java.util.Map;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

import eu.musaproject.multicloud.models.graphs.Peer;
import eu.musaproject.multicloud.models.graphs.Relationship;
import eu.musaproject.multicloud.models.graphs.graphModel;
import eu.musaproject.multicloud.models.macm.macmPeerType;
import eu.musaproject.multicloud.models.macm.macmRelationshipType;

public class iotRead extends graphModel {
	public void read_iot_graph() 
	{
		Driver driver;
		//Map<String, Peer> Peers;
		//List<Relationship> Rels;
		
		driver = GraphDatabase.driver( "bolt://localhost:7687", AuthTokens.basic("neo4j", "max"));
		Session session=driver.session();
		Peer p;
		String statement="";
		StatementResult result=null;
		
		//Create IoTGateway peers (gateway)
		statement ="MATCH (GW:iotgw:iaas:hw) return GW.name AS name,GW.protocol as protocol,GW.type AS type";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addGW(record.get("name").asString(),record.get("protocol").asString(),record.get("type").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());	
    	    		//System.out.println("TEST: "+p.getProperty("protocol"));
    	    }
    	}
		
		//Create Gateway peers (router)
		statement ="MATCH (Router:iotgw:iaas) return Router.name AS name,Router.protocol as protocol,Router.type AS type";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addGW(record.get("name").asString(),record.get("protocol").asString(),record.get("type").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());	
    	    }
    	}
		
		
		
		//Create IotDevice peers (smartmeter)
		statement ="MATCH (sm:iotdevice:iaas:hw:embedded) return sm.name AS name,sm.protocol as protocol,sm.type AS type";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addSM(record.get("name").asString(),record.get("protocol").asString(),record.get("type").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());	
    	    }
    	}
		
		//Create Network peers (WAN,LAN,Radio)
		statement ="MATCH (net:network) return net.name AS name,net.protocol as protocol,net.type AS type";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addNet(record.get("name").asString(),record.get("protocol").asString(),record.get("type").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());	
    	    }
    	}
		
		//Create Software peers (Agent,Mysql,Emoncms,Driver)
		statement ="MATCH (sw:software:saas) return sw.name AS name,sw.protocol as protocol,sw.type AS type  ";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addSW(record.get("name").asString(),record.get("protocol").asString(),record.get("type").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());	
    	    }
    	}
		
		//Create Platform peers (PHP)
		statement ="MATCH (pf:platform:paas) return pf.name AS name,pf.protocol as protocol,pf.type AS type";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addPF(record.get("name").asString(),record.get("protocol").asString(),record.get("type").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());	
    	    }
    	}
		
		//Create Physical Entity peers (Elettrodomestico)
		statement ="MATCH (pe:physical_entity) return pe.name AS name,pe.protocol as protocol,pe.type AS type";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    p=addPE(record.get("name").asString(),record.get("protocol").asString(),record.get("type").asString());
    	    List<String> properties=record.keys();
    	    for (String el: properties) {
    	    		p.setProperty(el, record.get(el).asString());	
    	    }
    	}
		
		//Create Connects relationships
		statement ="MATCH (gw) -[c:connects]->(n) return gw.name AS gw, n.name AS network, c.protocol AS protocol";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    addProvides(record.get("gw").asString(),record.get("network").asString(),record.get("protocol").asString());
    	   
    	}
		
		//Create Hosts relationships
		statement ="MATCH (gw) -[c:hosts]->(n) return gw.name AS gw, n.name AS software";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    addHosts(record.get("gw").asString(),record.get("software").asString());
    	}
		
		//Create Uses relationships
		statement ="MATCH (sw) -[c:uses]->(n) return sw.name AS sw, n.name AS software, c.protocol as protocol, c.type as type";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    addUses(record.get("sw").asString(),record.get("software").asString(),record.get("protocol").asString(),record.get("type").asString());
    	}
		
		//Create Monitors relationships
		statement ="MATCH (gw) -[c:monitors]->(n) return gw.name AS gw, n.name AS physical, c.protocol as protocol, c.type as type";
		result=session.run(statement);
		while ( result.hasNext() )
    	{
    	    Record record = result.next();
    	    addMonitors(record.get("gw").asString(),record.get("physical").asString(),record.get("protocol").asString(),record.get("type").asString());
    	}
		
		
	}
	
	public Peer addGW(String name, String protocol, String type){
		Peer p= new Peer(name,iotPeerType.GATEWAY);
		p.setProperty("protocol", protocol);
		p.setProperty("type", type);
		getPeers().put(name,p);
		return p;
	};
	
	public Peer addSM(String name,String protocol, String type){
		Peer p= new Peer(name,iotPeerType.SMARTMETER);
		p.setProperty("protocol", protocol);
		p.setProperty("type", type);
		getPeers().put(name,p);
		return p;
	};
	
	public Peer addNet(String name, String protocol, String type){
		Peer p= new Peer(name,iotPeerType.NETWORK);
		p.setProperty("protocol", protocol);
		p.setProperty("type", type);
		getPeers().put(name,p);
		return p;
	};
	
	public Peer addSW(String name,String protocol,String type){
		Peer p= new Peer(name,iotPeerType.SOFTWARE);
		p.setProperty("protocol", protocol);
		p.setProperty("type", type);
		getPeers().put(name,p);
		return p;
	};

	public Peer addPF(String name, String protocol,String type){
		Peer p= new Peer(name,iotPeerType.PLATFORM);
		p.setProperty("protocol", protocol);
		p.setProperty("type", type);
		getPeers().put(name,p);
		return p;
	};
	
	public Peer addPE(String name,String protocol,String type){
		Peer p= new Peer(name,iotPeerType.PHYSICALENTITY);
		p.setProperty("protocol", protocol);
		p.setProperty("type", type);
		getPeers().put(name,p);
		return p;
	};
	
	public Relationship addProvides(String csp, String service,String protocol) {
		Peer start=getPeers().get(csp);
		Peer stop=getPeers().get(service);
		Relationship rel= new Relationship(start,stop,iotRelationshipType.CONNECTS);
		rel.setProperty("protocol", protocol);
		Rels.add(rel);
		return rel;

	}
	
	public Relationship addHosts(String csp, String service) {
		Peer start=getPeers().get(csp);
		Peer stop=getPeers().get(service);
		Relationship rel= new Relationship(start,stop,iotRelationshipType.HOSTS);
		Rels.add(rel);
		return rel;

	}
	
	public Relationship addUses(String csp, String service,String protocol,String type) {
		Peer start=getPeers().get(csp);
		Peer stop=getPeers().get(service);
		Relationship rel= new Relationship(start,stop,iotRelationshipType.USES);
		rel.setProperty("protocol", protocol);
		rel.setProperty("type", type);
		Rels.add(rel);
		return rel;

	}	
	
	public Relationship addMonitors(String csp, String service,String protocol,String type) {
		Peer start=getPeers().get(csp);
		Peer stop=getPeers().get(service);
		Relationship rel= new Relationship(start,stop,iotRelationshipType.MONITORS);
		rel.setProperty("protocol", protocol);
		rel.setProperty("type", type);
		Rels.add(rel);
		return rel;

	}	
	
	public Map<String, Peer> peer(){
		return getPeers();
	}

}
